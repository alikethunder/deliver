const {
  app,
  BrowserWindow,
  ipcMain,
  Tray,
  Menu
} = require('electron');

const express = require('express');
const serv = express();
const http = require('http').Server(serv);
const io = require('socket.io')(http);
const ip = require('ip');

serv.get('/', function (req, res) {
  res.sendFile(`${__dirname}/page.html`);
});

serv.use(express.static(`${__dirname}/public`));

http.listen(0, function () {
  //console.log('Listening', http.address().port)
});

///////////////////reload in development mode
//consolerequire('electron-reload')(__dirname, {
  //electron: require(`${__dirname}/node_modules/electron`)
//});

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    },
    autoHideMenuBar: true,
    icon: `${__dirname}/public/icon/icon.png`
  })

  win.loadFile('index.html');

  //win.webContents.openDevTools();
  //console.log(ip.address());

  win.webContents.on('did-finish-load', () => {
    win.webContents.send('address', `${ip.address()}:${http.address().port}`);
  })
  

  io.on('connection', (socket) => {
    //console.log('a user connected', socket.id);

    socket.on('new peer', (id) => {
      //console.log('new peer ', id);
      win.webContents.send('new peer', id)
    });

    socket.on('disconnect', () => {
      //console.log(`socket ${socket.id} disconnected`);
      socket.disconnect(true);
      win.webContents.send('disconnect', socket.id);
    });

    ipcMain.on('send signaling', (e, data) => {
      if (socket.id == data.id) {
        //console.log('send signal ', data.id, socket.id);
        //io.emit('signal', data.data);
        socket.emit('signal', data);
      }
    });
    
    socket.on('send signal', (data) => {
      //console.log(data);
      //console.log('got signal', data.id);
      win.webContents.send('signaling', data)
    });

  });

  let tray = createTray();
  win.on('minimize', function (event) {
      event.preventDefault();
      win.hide();
      tray = tray || createTray();
  });

  win.on('restore', function (event) {
    win.show();
  });


  function createTray() {
    let appIcon = new Tray(`${__dirname}/public/icon/favicon.ico`);
    const contextMenu = Menu.buildFromTemplate([
        {
            label: 'Show', click: function () {
              win.show();
            }
        },
        {
            label: 'Exit', click: function () {
                app.isQuiting = true;
                app.quit();
            }
        }
    ]);

    appIcon.on('double-click', function (event) {
        win.show();
    });
    appIcon.setToolTip('Deliver');
    appIcon.setContextMenu(contextMenu);
    return appIcon;
  }
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})