const {
  desktopCapturer,
  ipcRenderer,
  shell
} = require('electron');

const peers = {};

const Peer = require('simple-peer');

//window.onload = function(){
  ipcRenderer.on('address', (e, address)=>{
    //console.log('address', e, address); 
    document.getElementById('address').innerHTML = address;
    //shell.openExternal(`http:\\${address}`);
  });
//}

desktopCapturer.getSources({
  types: ['screen']
}).then(async sources => {
  for (const source of sources) {
    if (source.name === 'Entire Screen') {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          audio: false,
          video: {
            mandatory: {
              chromeMediaSource: 'desktop',
              //chromeMediaSourceId: source.id,
              //minWidth: 1280,
              maxWidth: 640,
              //minHeight: 720,
              //maxHeight: 720
            }
          }
        })
        handleStream(stream)
      } catch (e) {
        handleError(e)
      }
      return
    }
  }
});

function handleStream(stream) {

  ipcRenderer.on('new peer', (e, _id) => {
    let id = _id;
    //console.log('new peer ', id, peers);

    peers[id] = new Peer({
      initiator: true,
      stream
    });

    peers[id].on('signal', (data) => {
      ipcRenderer.send('send signaling', {
        id,
        data
      });
    });

    peers[id].on('close', () => {
      //console.log('close');
      peers[id] && peers[id].destroy();
      delete peers[id];
    });
  });

  ipcRenderer.on('signaling', (e, data) => {
    //console.log(data)
    peers[data.id].signal(data.data);
  });

  ipcRenderer.on('disconnect', (e, id) => {
    //console.log('disconnect ', id);
    peers[id] && peers[id].destroy();
    delete peers[id];
  });

  //const video = document.getElementById('video');
  //video.srcObject = stream;
  //video.onloadedmetadata = (e) => video.play();
  //open some url
  //shell.openExternal('192.168.1.8:3000') 
}

function handleError(e) {
  //console.log(e);
}